/* eslint-disable no-dupe-keys */
/* eslint-disable no-unused-vars */
/* eslint-disable import/no-unresolved */
/* eslint-disable import/no-extraneous-dependencies */
const { defineConfig } = require('cypress');
const preprocessor = require('@badeball/cypress-cucumber-preprocessor');
const browserify = require('@badeball/cypress-cucumber-preprocessor/browserify');
const { addMatchImageSnapshotPlugin } = require('cypress-image-snapshot/plugin');
const allureWriter = require('@shelex/cypress-allure-plugin/writer');

async function setupNodeEvents(on, config) {
  await preprocessor.addCucumberPreprocessorPlugin(on, config);

  on('file:preprocessor', browserify.default(config));
  addMatchImageSnapshotPlugin(on, config);
  allureWriter(on, config);

  return config;
}

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    setupNodeEvents,
    baseUrl: 'https://juice-shop.herokuapp.com/#',
    experimentalWebKitSupport: true,
    specPattern: 'cypress/e2e/specs/{ui,api}/*.feature',
    viewportWidth: 1280,
    viewportHeight: 800,
    pageLoadTimeout: 200000,
    defaultCommandTimeout: 100000,
    retries: 3,
    video: false,
  },
});
