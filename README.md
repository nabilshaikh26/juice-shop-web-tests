# OWASP Juice Shop Tests

This Cypress + Cucumber based sample test automation project is implemented with the help of JavaScript and uses the [BDD](https://cucumber.io/docs/bdd/) approach. This project aims to test the [customer feedback](https://juice-shop.herokuapp.com/#/contact) page of the [juice-shop](https://juice-shop.herokuapp.com/#/) website. To learn more about this web application, you can visit the official [GitHub](https://github.com/juice-shop/juice-shop#readme) page.

## Description

#### Project Folder Structure:

```
├── allure-report (folder to support and generate allure based HTML report)
├── allure-results (contains all the tests results in json format)
├── cypress
│   └── /e2e/specs (feature files)
│   └── /e2e/step-definitions (tests code)
│   └── /e2e/ui-identifiers (all page's elements/locators goes here)
│   └── /fixtures (All test data goes here)
│   └── /screenshots (capture screenshots on failure)
│   └── /snapshots (reference snapshot placeholder to support visual testing)
│   └── /support (contains all the supporting test files)
├── cypress.config.js (cypress global configuration)
├── cypress.env.json (cypress environment variables)
├── run-tests.js (contains logic to run selected tests based on tags)

```
Note:<br> 
1. The <ins>allure-report</ins> & <ins>allure-results</ins> directories get created only after the first test run, and these two will get reset and recreated each time the test execution starts.<br>
2. As defined in gitlab's yaml file, it runs only specific tests in each pipeline stage based on tags.<br>
        <ul>
        <li>Devint: run those tests that are marked as `@Snapshot` and `@API`.</li>
        <li>QA: run those tests that are marked as `@Smoke`.</li>
        <li>Staging: run all tests by default.</li>
        </ul>

#### Salient Features:

- Implemented using the latest version of Cypress [(v12.12.0)](https://docs.cypress.io/guides/references/changelog#12-12-0) as of today.
- Based on Cucumber / Gherkin standard.
- Cross-browser support (including [Safari](https://docs.cypress.io/guides/guides/launching-browsers?ref=cypress.io#WebKit-Experimental) browser).
- Support various viewports such as desktop, tablet, and mobile.
- Support [visual testing](https://docs.cypress.io/guides/tooling/visual-testing).
- Based on POM design pattern.
- Fully automated and provides both console and HTML report using [Allure](https://www.npmjs.com/package/@shelex/cypress-allure-plugin).
- Ability to take screenshots on failure.
- Ability to attach failed step screenshot on allure report.
- Ability to retry failed tests to help reduce test flakiness.
- All tests are configured on [CI/CD pipeline](https://gitlab.com/nabilshaikh26/juice-shop-web-tests/-/pipelines), hence doesn't require any manual intervention to run tests. The job generates the test report (allure-based HTML report) in the form of an [artifact](https://gitlab.com/nabilshaikh26/juice-shop-web-tests/-/artifacts). You can [download](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#download-job-artifacts) job artifacts or view the job archive.
- It can run tests based on tags in each pipeline stage. This helps the developer to deliver a faster and quality build for testing on lower environments.
- Containerized the tests using Docker. It extends the ['cypress/included'](https://hub.docker.com/r/cypress/included) base image, and run tests using a [single docker command](https://www.cypress.io/blog/2019/05/02/run-cypress-with-a-single-docker-command/).

### But, why BDD?

Behaviour-Driven Development (BDD) is a way for software teams to work that closes the gap between business people and technical people by increasing the collaboration so that they can manage and deliver software development projects more effectively. BDD ensures that the development projects remain focused on the actual needs of the business while, at the same time, meeting the requirements of the user.

The BDD approach is often divided into two main parts:

- The first part involves using examples that are written in ubiquitous language as a way to illustrate behaviors or the different ways users interact with the product.
- The second part is the practice of utilizing those examples as a basis for automated tests. In addition to allowing developers to check functionality for the user, it also ensures that the overall system works precisely as defined by the business for the project’s entire lifetime.

...You can also check the [documentation](https://cucumber.io/docs/bdd/) to learn more about the BDD approach.


<right><p align="right">(<a href="#owasp-juice-shop-tests">back to top</a>)</p></right>

## Getting Started

### Installation:

** *Please read the [prerequisites](https://docs.cypress.io/guides/getting-started/installing-cypress#System-requirements) if you encounter any issues during installation.*

1. Clone the project.
2. Open the project using [Visual Studio Code](https://code.visualstudio.com/download) or any IDE of your choice.
3. Now, go to terminal and run `npm install`.<br>
*(Note: In case of an [upstream dependency conflict](https://github.com/jaredpalmer/cypress-image-snapshot/issues/231) error, run `npm install --force` instead. Though this is not a recommended step, however, this workaround works fine for a quick turnaround).*

### How To Run Tests?

(A) Headed Mode:

- To run tests in browser/headed mode, run `npm run cy:open` command in terminal. This will open an interactive cypress test runner.

(B) Headless Mode:

- To run tests in headless mode, <br>
        * `npm run cy:run` - run tests locally. <br>
        * `npm run cy:run:report` - run tests locally + generates the allure report. <br>
        * `npm run cy:run:report:open` - run tests locally + generates the allure report + opens the report automatically in browser using [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer). <br>
        * `npm run cy:docker` - run tests in Docker container. <br>
        * `npm run cy:docker:allure:generate` - run tests in Docker container + generates the allure report. <br>
        * `npm run cy:docker:allure:open` - run tests in Docker container + generates the allure report + opens the report automatically in browser using [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer). <br>
        **(To view all possible scripts, navigate to the 'package.json' file in the project).**

<ins>Note</ins>:-<br>
1) Please make sure Docker client is installed and running on your machine when running tests via docker.
2) To view allure based HTML report, expand `allure-report` directory and open the `index.html` file using Live Server.</br>

Once the test execution is complete, this is how the test report(s) would look like,

<ins>Console report</ins>:

<kbd><img src="/uploads/873b8d78a020f212488dc5eb5d23de81/Test-Pass-1.png" alt="Console Report" border="1" width=800></kbd>

<ins>HTML report - Pass</ins>: 

<kbd><img src="/uploads/c951d5dafe27f68932b0932479b0008c/Test-Pass-2.png" alt="HTML Report" border="1" width=800></kbd>

<kbd><img src="/uploads/3a1844f548d644386113c84fd7dface9/Test-Pass-3.png" alt="HTML Report" border="1" width=800></kbd>

<ins>HTML report - Fail</ins>: 

<kbd><img src="/uploads/339089af5ab77a7e8074c00f179e79bc/Test-Fail-1.png" alt="HTML Report" border="1" width=800></kbd>

<kbd><img src="/uploads/56250c1ef83485cb7916edf835dbd67d/Test-Fail-2.png" alt="HTML Report" border="1" width=800></kbd>

<br>

<right><p align="right">(<a href="#owasp-juice-shop-tests">back to top</a>)</p></right>

## Testing strategy

The goal of test automation is to increase the effectiveness and efficiency of testing. Good automation makes testing faster, more systematic, and reduces human error. This includes: to reduce the number of test cases that testers have to perform manually as well as those that are challenging to perform manually, therefore saving time and effort.

The ideal test automation strategy is to follow the [Test Pyramid](https://martinfowler.com/articles/practical-test-pyramid.html) mindset. Though, this project is simply for a demonstration purpose. However, the test pyramid is a way of thinking about how different kinds of automated tests should be used to create a balanced portfolio. The whole point is to offer immediate feedback to ensure that code changes do not disrupt existing features. This would essentially help both developers and QAs to create high-quality software. It reduces the time required for developers to identify if a change they introduced breaks the code.

This test automation pyramid mainly operates at three levels: Unit, Integration & UI.

<kbd><img src="/uploads/92f7379eda6254785870e218fc0f48e2/Test-pyramid.png" alt="Test Pyramid" border="1" width=500></kbd>

1. <ins>**Unit tests**</ins> form the base of the test pyramid. They should be frequent, and they should run fast.

2. <ins>**Integration tests**</ins> are the middle tier of the pyramid. These tests focus on interactions of your code with the outside world, such as databases and external services or microservices.

3. <ins>**UI tests**</ins> top the test pyramid. They’re written from the perspective of a user and should test that your entire application is functioning from front-end to back-end.

<br>

**Why to use test pyramid?**

- Pyramid help QAs to define right priorities. If test scripts are written with a greater focus on the UI, then chances are that core business logic and back-end functionality is not sufficiently verified periodically. Hence, this affects product quality and leads to more work for the team.
- As TAT (turnaround time) of UI tests is high, it leads to lower test coverage overall. By adopting the pyramid, such situations can be completely avoided, and QAs can focus on writing quality tests keeping in mind all the 3 layers defined.
- Frequent collaboration between 3 Amigos (i.e. Developer, Tester & Product Owner)
- Since the pyramid is built to run the complex tests at the beginning, testers can manage time better, get better results and essentially make life easier for everyone involved. Therefore, it emphasize speed and efficacy.

<right><p align="right">(<a href="#owasp-juice-shop-tests">back to top</a>)</p></right>

## Miscellaneous

**Some of the best practices writing features:**

1. Each scenario should be short & independent of each other. If the scenario is bit complex, split them into two.
2. The scenarios should be concise and to the point, so that the reader can quickly grasp the intent of the test.
3. Avoid “I” in step definitions. It is recommend writing steps in third person as this will remind you about the user's role in the application. 
4. Don’t use both first-person and third-person pronouns together in one scenario.
5. When you encounter a step which contains two actions, break them into two using 'And' keyword. There may be reasons for conjunctive steps. However, most of the time it’s better to avoid them.
6. If you are using the same steps at the beginning of all scenarios of a feature, put them into the feature’s Background as Background steps are run before each scenario. However, if you want to run a block of code before/after every scenario then cucumber hooks (such as `@Before` & `@After`) would be the ideal choice.
7. Make use of 'Scenario Outline' if there is a need to re-run the same scenario on multiple test data. But scenario outlines should focus on one behaviour at a time and use only the necessary variations.
8. Cucumber features/scenarios should be tagged properly so that they can be selected / grouped for automated test runs.

<right><p align="right">(<a href="#owasp-juice-shop-tests">back to top</a>)</p></right>

## Contact

<a href="mailto:nabilshaikh26@gmail.com"><img src="https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white" /> &nbsp; <a href="https://www.github.com/nabilshaikh"><img src="https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white"/> &nbsp; <a href="https://www.gitlab.com/nabilshaikh26"><img src="https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white"/> &nbsp; <a href="https://www.linkedin.com/in/nabil-shaikh-5362b71b3/"><img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white"/>

<right><p align="right">(<a href="#owasp-juice-shop-tests">back to top</a>)</p></right>

## Contributing

Contributions are what make the open source community such an amazing place to learn and inspire. Any contributions you make are greatly appreciated.<br>

If you have any suggestions that would make this tests better, please fork the repo and create a merge request. You can also simply open an issue with the tag "enhancement". Thanks again!

1. Fork the Project
2. Create either the feature branch `(git checkout -b feature/amazing-feature)` or test branch `(git checkout -b test/amazing-test)`
3. Commit your changes `(git commit -m 'Add some amazing feature')`
4. Push to the branch `(git push origin feature/amazing-feature)`
5. Open a Merge Request

<right><p align="right">(<a href="#owasp-juice-shop-tests">back to top</a>)</p></right>

## Acknowledgments

Following is the list of resources that you may find helpful:

<ul>
<li>[Cypress](https://docs.cypress.io/guides/end-to-end-testing/writing-your-first-end-to-end-test)</li>
<li>[Cypress Cucumber Preprocessor](https://github.com/badeball/cypress-cucumber-preprocessor)</li>
<li>[Cypress Browserify Preprocessor](https://www.npmjs.com/package/@cypress/browserify-preprocessor)</li>
<li>[Cypress Image Snapshot](https://www.npmjs.com/package/cypress-image-snapshot)</li>
<li>[Cypress Allure](https://www.npmjs.com/package/@shelex/cypress-allure-plugin)</li>
<li>[Cypress GitLab](https://docs.cypress.io/guides/continuous-integration/gitlab-ci)</li>
<li>[ESLint](https://github.com/cypress-io/eslint-plugin-cypress)</li>
<li>[Experimental support for WebKit](https://docs.cypress.io/guides/guides/launching-browsers#WebKit-Experimental)</li>
<li>[The Practical Test Pyramid](https://martinfowler.com/articles/practical-test-pyramid.html)</li>
</ul>

<right><p align="right">(<a href="#owasp-juice-shop-tests">back to top</a>)</p></right>
