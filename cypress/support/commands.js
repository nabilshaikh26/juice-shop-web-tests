/* eslint-disable no-undef */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable import/no-unresolved */
// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
/// <reference types="cypress" />

import { addMatchImageSnapshotCommand } from 'cypress-image-snapshot/command';
import Contact from '../e2e/ui-identifiers/Contact';

addMatchImageSnapshotCommand();
const contact = new Contact();

Cypress.Commands.add('selectFeedbackRating', (rating) => {
  switch (rating) {
    case '1':
      contact.getFeedbackFormRatingSlider().focus().type('{rightarrow} {leftarrow}');
      break;
    case '2':
      contact.getFeedbackFormRatingSlider().focus().type('{rightarrow}');
      break;
    case '3':
      contact.getFeedbackFormRatingSlider().focus().type('{rightarrow}{rightarrow}');
      break;
    case '4':
      contact.getFeedbackFormRatingSlider().focus().type('{rightarrow} {rightarrow} {rightarrow}');
      break;
    case '5':
      contact.getFeedbackFormRatingSlider().focus().type('{rightarrow} {rightarrow} {rightarrow} {rightarrow}');
      break;
    default:
      contact.getFeedbackFormRatingSlider().focus().type('{rightarrow} {rightarrow}');
      cy.log('Invalid rating input! Select a range between 1 to 5. Default rating 3 has been set');
  }
});

Cypress.Commands.add('visitPage', (pageName, device, language) => {
  switch (device) {
    case 'mobile':
      cy.log('Opening mobile...');
      cy.viewport('iphone-6'); // {width-375 & height-667}
      cy.setCookie('welcomebanner_status', 'dismiss');
      cy.setCookie('cookieconsent_status', 'dismiss');
      cy.setCookie('language', Cypress.env(language));
      cy.visit(pageName).then(() => {
        cy.getCookie('language', { timeout: 2000 }).should('have.property', 'value', Cypress.env(language));
      });
      break;
    case 'tablet':
      cy.log('Opening tablet...');
      cy.viewport('ipad-2'); // {width-768 & height-1024}
      cy.setCookie('welcomebanner_status', 'dismiss');
      cy.setCookie('cookieconsent_status', 'dismiss');
      cy.setCookie('language', Cypress.env(language));
      cy.visit(pageName).then(() => {
        cy.getCookie('language', { timeout: 2000 }).should('have.property', 'value', Cypress.env(language));
      });
      break;
    case 'desktop':
      cy.log('Opening desktop...');
      cy.viewport('macbook-15'); // {width-1440 & height-900}
      cy.setCookie('welcomebanner_status', 'dismiss');
      cy.setCookie('cookieconsent_status', 'dismiss');
      cy.setCookie('language', Cypress.env(language));
      cy.visit(pageName).then(() => {
        cy.getCookie('language', { timeout: 2000 }).should('have.property', 'value', Cypress.env(language));
      });
      break;
    default:
      cy.log('Starting test in default viewport 1280x800');
  }
});
