module.exports = {
  REST_SERVICE_CAPTCHA: 'https://juice-shop.herokuapp.com/rest/captcha/',
  REST_SERVICE_LANGUAGES: 'https://juice-shop.herokuapp.com/rest/languages',
  REST_SERVICE_USER: 'https://juice-shop.herokuapp.com/rest/user/whoami',
  API_SERVICE_FEEDBACKS: 'https://juice-shop.herokuapp.com/api/Feedbacks/',
};
