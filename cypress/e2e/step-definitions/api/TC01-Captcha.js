import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';

Given('the base url {string}', (url) => {
  cy
    .visit(url);
});

When('I make HTTP {string} request to captcha endpoint {string}', (method) => {
  cy
    .get('@captchaResult', { timeout: 2000 })
    .its('request.method')
    .should('eq', method);
});

Then('the response status should equals {int}', (statusCode) => {
  cy
    .get('@captchaResult', { timeout: 2000 })
    .its('response.statusCode')
    .should('eq', statusCode);
});

Then('response payload should have the following {int} properties', (propertyCount) => {
  cy
    .get('@captchaResult', { timeout: 2000 })
    .its('response.body')
    .as('captchaResponseBody');
  cy
    .get('@captchaResponseBody', { timeout: 2000 })
    .should(() => {
      expect(JSON.stringify.length).to.equal(propertyCount);
    });
});

Then('verify {string} key to be of {string} type, and it should equals value {int}', (key, type, value) => {
  cy
    .get('@captchaResponseBody')
    .its(key)
    .should((element) => {
      expect(element).to.be.a(type);
      expect(element).equal(value);
    });
});

Then('verify {string} key to be of {string} type, and it should equals value {string}', (key, type, value) => {
  cy
    .get('@captchaResponseBody')
    .its(key)
    .should((element) => {
      expect(element).to.be.a(type);
      expect(element).equal(value);
    });
});
