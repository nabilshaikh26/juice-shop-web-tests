/* eslint-disable no-undef */
/* eslint-disable func-names */
/// <reference types="cypress" />
/* eslint-disable max-len */
import { When, Then } from '@badeball/cypress-cucumber-preprocessor';
import Home from '../../ui-identifiers/Home';
import Contact from '../../ui-identifiers/Contact';
import { API_SERVICE_FEEDBACKS } from '../../../support/global-constant';

const home = new Home();
const contact = new Contact();

// Scenario 1

When('anonymous user goes to the side-navigation menu present on the top-left of screen', () => {
  home
    .getSideNavigationMenu()
    .click();
});

Then('anonymous user should see the {string} sub-category to be present under the {string} category', (subcategoryText, categoryText) => {
  home
    .getSubcategoryCustomerFeedback()
    .then((subcategoryCustomerFeedback) => {
      expect(subcategoryCustomerFeedback.text().trim()).to.equal(subcategoryText);
    });
  home
    .getCategoryContact()
    .should('have.text', categoryText)
    .then((categoryContact) => {
      expect(categoryContact.next().text()).to.contain(subcategoryText);
    });
});

// Scenario 2 & Scenario 3

When("anonymous user chooses the 'Customer Feedback' option present under the side-navigation menu", () => {
  home
    .getSideNavigationMenu()
    .click();
  home
    .getSubcategoryCustomerFeedback()
    .click();
});

Then('anonymous user should navigate to the {string} page of the {string} form', (pageName, formTitle) => {
  cy
    .url()
    .should('include', pageName);
  contact
    .getFeedbackFormHeader()
    .then((getFormTitle) => {
      expect(getFormTitle.text().trim()).to.equal(formTitle);
    });
});

// Scenario 4

Then('anonymous user should see the customer feedback form to be rendered on {string} in various {string}', (viewport, language) => {
  contact
    .getFeedbackFormCard()
    .matchImageSnapshot(
      `customer-feedbackform-${viewport}-${language}`,
      {
        failureThreshold: 0.02,
        failureThresholdType: 'percent',
        dumpDiffToConsole: true,
        capture: 'viewport',
      },
    );
});

// Scenario 5

When('anonymous user should see the Submit button to be disabled by default', () => {
  contact
    .getFeedbackFormSubmitButton()
    .should('be.disabled');
});

When('anonymous user enters the complete information - comment as {string}, rating as {string}, and CAPTCHA response as {int}', (comment, rating, captcha) => {
  contact
    .getFeedbackFormCommentTextarea()
    .type(comment);
  cy
    .selectFeedbackRating(rating);
  contact
    .getFeedbackFormCaptchaTextInput()
    .type(captcha);
});

Then('anonymous user should see the Submit button to be enabled', () => {
  contact
    .getFeedbackFormSubmitButton()
    .should('be.enabled');
});

// Scenario 6

Then('anonymous user sees the following fields to be mandatory & non-mandatory on the customer feedback form:', () => {
  contact
    .getFeedbackFormAuthorTextInput()
    .should('be.visible');
  contact
    .getFeedbackFormCommentTextarea()
    .should('be.visible');
  contact
    .getFeedbackFormRatingSlider()
    .should('be.visible');
  contact
    .getFeedbackFormCaptchaTextInput()
    .should('be.visible');
});

Then('Author as non-mandatory', () => {
  contact
    .getFeedbackFormAuthorTextInput()
    .invoke('attr', 'aria-required')
    .should('equal', 'false');
});

Then('Comment as mandatory', () => {
  contact
    .getFeedbackFormCommentTextarea()
    .invoke('attr', 'aria-required')
    .should('equal', 'true');
});

Then('CAPTCHA as mandatory', () => {
  contact
    .getFeedbackFormCaptchaTextInput()
    .invoke('attr', 'aria-required')
    .should('equal', 'true');
});

Then('Rating as non-mandatory. Though the rating field looks non-mandatory, but, it has the minimum input value of {string} which makes it mandatory in nature, i.e. user has to select atleast 1 rating value out of 5 scale', (minValue) => {
  contact
    .getFeedbackFormRatingSlider()
    .invoke('attr', 'aria-required')
    .should('not.exist');
  contact
    .getFeedbackFormRatingSlider()
    .invoke('attr', 'aria-valuemin')
    .should('equal', minValue);
});

// Scenario 7

Then('anonymous user should see the author name to be prepopulated as {string} on the feedback form', (userType) => {
  contact
    .getFeedbackFormAuthorTextInput()
    .invoke('removeAttr', 'disabled')
    .focus()
    .invoke('val')
    .then((author) => {
      expect(author).to.equal(userType);
    });
});

// Scenario 8

Then('registered user should see the author name {string} to be prepopulated in obscured form as {string}, and not {string}', (authorEmailID, obscuredAuthorEmailID, anonymousText) => {
  contact
    .getFeedbackFormAuthorTextInput()
    .invoke('removeAttr', 'disabled')
    .focus()
    .invoke('val')
    .then((author) => {
      expect(author).to.equal(obscuredAuthorEmailID);
      expect(author).to.not.equal(anonymousText);
    });
  cy
    .wait('@user', { timeout: 10000 })
    .then((resp) => {
      expect(resp.response.body.user.email).to.equal(authorEmailID);
    });
});

// Scenario 9

Then('the user should see the following placeholder text implemented on Comment & CAPTCHA textboxes', () => {
  contact
    .getFeedbackFormCommentTextarea()
    .should('be.visible');
  contact
    .getFeedbackFormCaptchaQuestion()
    .should('be.visible');
});

When('Comment textarea with placeholder text as {string}, which has the maximum length of {string} characters', (placeholderText, maxCharacterLimit) => {
  contact
    .getFeedbackFormCommentTextarea()
    .invoke('attr', 'placeholder')
    .then((commentPlaceholderText) => {
      expect(commentPlaceholderText).to.eq(placeholderText);
    });
  contact
    .getFeedbackFormCommentTextarea()
    .invoke('attr', 'maxlength')
    .then((commentMaxLength) => {
      expect(commentMaxLength).to.eq(maxCharacterLimit);
    });
});

Then('CAPTCHA textbox with placeholder text as {string}', (placeholderText) => {
  contact
    .getFeedbackFormCaptchaTextInput()
    .invoke('attr', 'placeholder')
    .then((captchaPlaceholderText) => {
      expect(captchaPlaceholderText).to.eq(placeholderText);
    });
});

// Scenario 10

When('anonymous user enters the comment as {string}', (commentFeedbackText) => {
  contact
    .getFeedbackFormCommentTextarea()
    .type(commentFeedbackText)
    .should('have.value', commentFeedbackText);
  contact
    .getFeedbackFormCommentTextarea()
    .invoke('val')
    .as('getUserCommentFeedbackText');
});

Then('anonymous user should see the feedback comment to be populated appropriately in the comment textarea', function () {
  contact
    .getFeedbackFormCommentTextarea()
    .invoke('val')
    .then((inputCommentFeedbackText) => {
      expect(inputCommentFeedbackText).to.equal(this.getUserCommentFeedbackText);
    });
});

When('anonymous user deletes the same comment', () => {
  contact
    .getFeedbackFormCommentTextarea()
    .clear();
});

Then('anonymous user should no longer see the comment feedback in the comment textarea', () => {
  contact
    .getFeedbackFormCommentTextarea()
    .blur();
  contact
    .getFeedbackFormCommentTextarea()
    .invoke('val')
    .should('be.empty');
  contact
    .getFeedbackFormCommentTextarea()
    .should('have.class', 'ng-invalid');
});

Then('anonymous user should see the error message as {string}', (errorMessage) => {
  contact
    .getFeedbackFormCommentTextarea()
    .click();
  contact
    .getFeedbackFormCommentTextareaErrorMessage()
    .invoke('text')
    .then((getErrorMessage) => {
      expect(getErrorMessage.trim()).to.equal(errorMessage);
    });
});

// Scenario 11, Scenario 12 & Scenario 13

When('anonymous user finds the warning label {string} present beneath the comment textarea', (maxCharacterLimitText) => {
  contact
    .getFeedbackFormCommentTextareaMaxCharacterLimitWarningLabel()
    .invoke('text')
    .then((getMaxCharacterLimitText) => {
      expect(getMaxCharacterLimitText.trim()).to.equal(maxCharacterLimitText);
    });
});

When('anonymous user should see the character count indicator to show {string}', (inputCharacterCount) => {
  contact
    .getFeedbackFormCommentTextareaCharacterCountIndicator()
    .invoke('text')
    .then((getInputCharacterCount) => {
      expect(getInputCharacterCount.trim()).to.equal(inputCharacterCount);
    });
});

When('if an anonymous user enters a blank space as a comment', () => {
  contact
    .getFeedbackFormCommentTextarea()
    .type(' ');
});

// Scenario 14

When('anonymous user enters the comment such that it exceeds 160 characters limit {string}', function (commentFeedbackText) {
  cy
    .wrap(commentFeedbackText)
    .as('getUserCommentEquals160Characters');
  contact
    .getFeedbackFormCommentTextarea()
    .type(this.getUserCommentEquals160Characters);
});

When('anonymous user should still see the character count indicator to show {string}, and the excess characters should trim off', function (inputCharactersCount) {
  contact
    .getFeedbackFormCommentTextareaCharacterCountIndicator()
    .invoke('text')
    .then((getInputCharactersCount) => {
      expect(getInputCharactersCount.trim()).to.equal(inputCharactersCount);
    });
  contact
    .getFeedbackFormCommentTextarea()
    .invoke('val')
    .then((inputCommentFeedbackText) => {
      expect(inputCommentFeedbackText.length).to.equal(160);
      expect(this.getUserCommentEquals160Characters.length).to.greaterThan(160);
      expect(inputCommentFeedbackText.length).to.be.lessThan(this.getUserCommentEquals160Characters.length);
    });
});

// Scenario 15

When('anonymous user performs the following operations to understand what appropriate feedback rating to be selected', () => {
  contact
    .getFeedbackFormRatingSlider()
    .should('be.visible');
});

When('on moving the rating slider to 1, the feedback value appears as {string}', (ratingValue) => {
  contact
    .getFeedbackFormRatingSlider()
    .focus()
    .type('{rightarrow} {leftarrow}');
  contact
    .getFeedbackFormRatingSlider()
    .invoke('attr', 'aria-valuenow')
    .should('equal', '1');
  contact
    .getFeedbackFormRatingSlider()
    .invoke('attr', 'aria-valuetext')
    .should('equal', ratingValue);
});

When('on moving the rating slider to 2, the feedback value appears as {string}', (ratingValue) => {
  contact
    .getFeedbackFormRatingSlider()
    .focus()
    .type('{rightarrow}');
  contact
    .getFeedbackFormRatingSlider()
    .invoke('attr', 'aria-valuenow')
    .should('equal', '2');
  contact
    .getFeedbackFormRatingSlider()
    .invoke('attr', 'aria-valuetext')
    .should('equal', ratingValue);
});

When('on moving the rating slider to 3, the feedback value appears as {string}', (ratingValue) => {
  contact
    .getFeedbackFormRatingSlider()
    .focus()
    .type('{rightarrow}');
  contact
    .getFeedbackFormRatingSlider()
    .invoke('attr', 'aria-valuenow')
    .should('equal', '3');
  contact
    .getFeedbackFormRatingSlider()
    .invoke('attr', 'aria-valuetext')
    .should('equal', ratingValue);
});

When('on moving the rating slider to 4, the feedback value appears as {string}', (ratingValue) => {
  contact
    .getFeedbackFormRatingSlider()
    .focus()
    .type('{rightarrow}');
  contact
    .getFeedbackFormRatingSlider()
    .invoke('attr', 'aria-valuenow')
    .should('equal', '4');
  contact
    .getFeedbackFormRatingSlider()
    .invoke('attr', 'aria-valuetext')
    .should('equal', ratingValue);
});

When('on moving the rating slider to 5, the feedback value appears as {string}', (ratingValue) => {
  contact
    .getFeedbackFormRatingSlider()
    .focus()
    .type('{rightarrow}');
  contact
    .getFeedbackFormRatingSlider()
    .invoke('attr', 'aria-valuenow')
    .should('equal', '5');
  contact
    .getFeedbackFormRatingSlider()
    .invoke('attr', 'aria-valuetext')
    .should('equal', ratingValue);
});

Then('anonymous user realizes {string} to be the maximum rating value to be selected', (maxValue) => {
  contact
    .getFeedbackFormRatingSlider()
    .invoke('attr', 'aria-valuemax')
    .should('equal', maxValue);
});

// Scenario 16

Then('anonymous user sees the CAPTCHA question along with an input textbox to enter the captcha value', () => {
  contact
    .getFeedbackFormCaptchaQuestion()
    .should('be.visible');
  contact
    .getFeedbackFormCaptchaTextInput()
    .should('be.visible')
    .and('be.enabled');
});

Then('anonymous user finds the CAPTCHA input textbox to accept only integers', () => {
  contact
    .getFeedbackFormCaptchaTextInput()
    .invoke('attr', 'pattern')
    .then((pattern) => {
      expect(pattern.trim()).to.match(/-?[\d]*/);
    });
});

// Scenario 17

Then('anonymous user enters an invalid random captcha response {string}', (response) => {
  contact
    .getFeedbackFormCaptchaTextInput()
    .type(response)
    .blur();
});

Then('it should throw an error {string}', (errorMessage) => {
  contact
    .getFeedbackFormCaptchaTextInputErrorMessage()
    .invoke('text')
    .then((getErrorMessage) => {
      expect(getErrorMessage.trim()).to.equal(errorMessage);
    });
});

// Scenario 18

Then('anonymous user enters a valid random captcha response {string}', (response) => {
  contact
    .getFeedbackFormCaptchaTextInput()
    .type(response)
    .blur();
});

Then('it should not throw an error {string}', (errorMessage) => {
  cy
    .contains(errorMessage)
    .should('not.exist');
});

// Scenario 19

Then('anonymous user sees the captcha question to appear on the customer feedback form', () => {
  contact
    .getFeedbackFormCaptchaQuestion()
    .should('be.visible')
    .then((question) => {
      cy
        .wrap(question.text().trim().length)
        .should('be.gte', 5);// .to.be.greaterThan(5, 6);
      cy
        .wrap(question.text().trim())
        .as('captchaQuestion');
    });
});

When('anonymous user reloads the contact page', () => {
  cy
    .reload();
});

Then('anonymous user should see the different captcha question to appear after the page reload', function () {
  contact
    .getFeedbackFormCaptchaQuestion()
    .should('be.visible')
    .then((newQuestion) => {
      cy.wait('@captcha').should((captchaAPI) => {
        expect(captchaAPI.response.statusCode).to.eq(200);
      });
      cy.wrap(newQuestion.text().trim().length).should('be.gte', 5);
      expect(newQuestion.text().trim()).to.not.equal(this.captchaQuestion);
    });
});

// Scenario 20

When('anonymous user decides to keep the comment section blank', () => {
  contact
    .getFeedbackFormCommentTextarea()
    .should('exist');
});

When('provides the feedback rating as {string} out of 5 scale', (rating) => {
  cy
    .selectFeedbackRating(rating);
});

When('answers the CAPTCHA with a correct response {string}', (captchaAnswer) => {
  contact
    .getFeedbackFormCaptchaTextInput()
    .type(captchaAnswer);
});

When('anonymous user should see the Submit button to be disabled', () => {
  contact
    .getFeedbackFormSubmitButton()
    .should('be.disabled');
});

// Scenario 21

When('anonymous user decides not to add the feedback rating', () => {
  contact
    .getFeedbackFormRatingSlider()
    .should('exist');
});

When('provides the comment as {string}', (inputText) => {
  contact
    .getFeedbackFormCommentTextarea()
    .type(inputText);
});

// Scenario 22

When('anonymous user decides not to answer the CAPTCHA', () => {
  contact
    .getFeedbackFormCaptchaTextInput()
    .should('exist');
});

// Scenario 23

When('anonymous user enters the following required information present on the feedback form:', () => {});

When('Comment as {string}', (comment) => {
  contact
    .getFeedbackFormCommentTextarea()
    .type(comment);
});

When('Rating as {string}', (rating) => {
  cy
    .selectFeedbackRating(rating);
});

When('correct CAPTCHA response as {string}', (captchaAnswer) => {
  contact
    .getFeedbackFormCaptchaTextInput()
    .type(captchaAnswer);
});

// Scenario 24

When('anonymous user clicks on the Submit button', () => {
  cy
    .intercept('POST', API_SERVICE_FEEDBACKS, { fixture: 'apiFeedbacks' })
    .as('formSuccess');
  contact
    .getFeedbackFormSubmitButton()
    .click();
});

Then('the feedback form should get submitted successfully and it should show the message as {string}', (successMessage) => {
  contact
    .getFeedbackFormSubmitSmackbarMessage()
    .should('have.text', successMessage);
});

// Scenario 25

When('anonymous user submits the feedbaack form', () => {
  cy
    .intercept('POST', API_SERVICE_FEEDBACKS)
    .as('apiSubmitFeedbackForm');
  contact
    .getFeedbackFormSubmitButton()
    .click();
});

Then('correct CAPTCHA response', () => {
  /* Method 1: This uses eval() method to execute the math argument
    contact.getFeedbackFormCaptchaQuestion().then((captcha) => {
        contact.getFeedbackFormCaptchaTextInput().type(eval(captcha.text().trim()));
    });
    */

  /* Method 2: This intercepts the actual '/captcha' api to get an answer out of it body */
  cy
    .wait('@captcha', { timeout: 10000 })
    .then((captureCaptcha) => {
      contact.getFeedbackFormCaptchaTextInput().type(captureCaptcha.response.body.answer);
    });
});

// Scenario 26

Then('incorrect CAPTCHA response {string}', (captcha) => {
  contact
    .getFeedbackFormCaptchaTextInput()
    .type(captcha);
});

Then('the feedback form should not get submitted and it should show an error message as {string}', (errorMessage) => {
  contact
    .getFeedbackFormSubmitSmackbarMessage()
    .should('have.text', errorMessage);
  cy
    .get('@apiSubmitFeedbackForm')
    .then((object) => {
      expect(object.response.body).to.equal(errorMessage);
      expect(object.response).to.have.property('statusCode', 401);
      expect(object.response).to.have.property('statusMessage', 'Unauthorized');
    });
});

Then('anonymous user finds the CAPTCHA input field to get reset', () => {
  contact
    .getFeedbackFormCaptchaTextInput()
    .should('be.empty')
    .and('have.class', 'ng-pristine');
});

// Scenario 27

Then('anonymous user finds the comment, rating, and CAPTCHA input fields to get reset after form submission', () => {
  contact
    .getFeedbackFormCommentTextarea()
    .should('be.empty');
  contact
    .getFeedbackFormRatingSlider()
    .invoke('attr', 'aria-valuenow')
    .should('equal', '0');
  contact
    .getFeedbackFormCaptchaTextInput()
    .should('be.empty');
});

Then('anonymous user should still see the author name as {string} in readonly state', (anonymous) => {
  contact
    .getFeedbackFormAuthorTextInput()
    .should('be.disabled');
  contact
    .getFeedbackFormAuthorTextInput()
    .invoke('removeAttr', 'disabled')
    .focus()
    .invoke('val')
    .should((author) => {
      expect(author).to.equal(anonymous);
    });
});

// Scenario 28

When('user submits the feedback form when the feedback service is down', () => {
  cy
    .intercept(API_SERVICE_FEEDBACKS, {
      statusCode: 503,
      method: 'get',
      response: {},
    })
    .as('feedbackServiceDown');
  contact
    .getFeedbackFormSubmitButton()
    .click();
});

When('the feedback form should not get submitted and the feedback service should return {int} error', (statusCode) => {
  cy
    .get('@feedbackServiceDown')
    .then((object) => {
      expect(object.response).to.have.a.property('statusCode', statusCode);
    });
});
