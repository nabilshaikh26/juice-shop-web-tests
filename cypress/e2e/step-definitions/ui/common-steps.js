import { Given, Before } from '@badeball/cypress-cucumber-preprocessor';
import { REST_SERVICE_CAPTCHA, REST_SERVICE_USER } from '../../../support/global-constant';

/**
  * @desc Generic steps
*/

Before(() => {
  cy
    .intercept(REST_SERVICE_CAPTCHA)
    .as('captcha');
});

const captchaResultAlias = 'captchaResult';
Before({ tags: '@StubCaptchaResult' }, () => {
  cy
    .intercept('GET', REST_SERVICE_CAPTCHA, { fixture: 'apiCaptcha.json' })
    .as(captchaResultAlias);
});

const userAlias = 'user';
Before({ tags: '@StubUser' }, () => {
  cy
    .intercept('GET', REST_SERVICE_USER, { fixture: 'apiUser.json' })
    .as(userAlias);
});

Given('anonymous user visits {string} page on {string} device in {string}', (pageName, device, language) => {
  cy
    .visitPage(pageName, device, language);
});

Given('registered user visits {string} page on {string} device in {string}', (pageName, device, language) => {
  cy
    .visitPage(pageName, device, language);
});
