Feature: CAPTCHA API

As a tester, I want to verify the CAPTCHA api response and its payload.

    Background: Initialize the feedback url
        Given the base url 'https://juice-shop.herokuapp.com/#/contact'
    
    @StubCaptchaResult @API
    Scenario: Verify the captcha response payload
        When I make HTTP 'GET' request to captcha endpoint '/rest/captcha'
        Then the response status should equals 200
        And response payload should have the following 3 properties
        * verify 'captchaId' key to be of 'number' type, and it should equals value 1234
        * verify 'captcha' key to be of 'string' type, and it should equals value '1+0+1'
        * verify 'answer' key to be of 'string' type, and it should equals value '2'