Feature: Customer's Feedback Functionality

As a product manager, I need a feedback functionality to be implemented on the website so that we can proactively collect both guest & registered customers' feedback to improve the user experience and their needs while using our services.

@1
Scenario Outline: Presence of a 'Customer Feedback' option inside the side-navigation menu
    Given anonymous user visits '/' page on '<viewport>' device in '<language>'
    When anonymous user goes to the side-navigation menu present on the top-left of screen
    Then anonymous user should see the 'Customer Feedback' sub-category to be present under the 'Contact' category

Examples:
    | viewport | language |
    | desktop  | english  |
    | tablet   | english  |
    | mobile   | english  |

@2
Scenario: Navigate to the customer feedback page using the side-navigation menu
    Given anonymous user visits '/' page on 'desktop' device in 'english'
    When anonymous user chooses the 'Customer Feedback' option present under the side-navigation menu
    Then anonymous user should navigate to the '/contact' page of the 'Customer Feedback' form

@3
Scenario: Navigate to the customer feedback page directly through URL
    When anonymous user visits '/contact' page on 'desktop' device in 'english'
    Then anonymous user should navigate to the '/contact' page of the 'Customer Feedback' form

@4 @StubCaptchaResult @Snapshot
Scenario Outline: Look & feel of customer feedback page in various languages
    When anonymous user visits '/contact' page on '<viewport>' device in '<language>'
    Then anonymous user should see the customer feedback form to be rendered on '<viewport>' in various '<language>'
  
Examples:
    |viewport |language |
    |desktop  |english  |
    |mobile   |english  |
    |desktop  |german   |
    |mobile   |german   |

@5
Scenario: Verify that the customer cannot submit the blank feedback form without entering the details
    Given anonymous user visits '/contact' page on 'desktop' device in 'english'
    And anonymous user should see the Submit button to be disabled by default
    When anonymous user enters the complete information - comment as 'abc', rating as '2', and CAPTCHA response as 123
    Then anonymous user should see the Submit button to be enabled
    #Note: The rating field is not marked as mandatory (i.e. without asterisk). As the minimum value set for this input field is 1, hence, it cannot be left blank.

@6
Scenario: Verify the mandatory & non-mandatory fields to be present on the customer feedback form
    When anonymous user visits '/contact' page on 'desktop' device in 'english'
    Then anonymous user sees the following fields to be mandatory & non-mandatory on the customer feedback form:
    * Author as non-mandatory
    * Comment as mandatory
    * CAPTCHA as mandatory
    * Rating as non-mandatory. Though the rating field looks non-mandatory, but, it has the minimum input value of '1' which makes it mandatory in nature, i.e. user has to select atleast 1 rating value out of 5 scale

@7
Scenario: Verify the author name to be prepopulated based on the user type (guest user)
    When anonymous user visits '/contact' page on 'desktop' device in 'english'
    Then anonymous user should see the author name to be prepopulated as 'anonymous' on the feedback form

@8 @StubUser
Scenario: Verify the author name to be prepopulated based on the user type (registered user)
    When registered user visits '/contact' page on 'desktop' device in 'english'
    Then registered user should see the author name 'nabil@gmail.com' to be prepopulated in obscured form as '***il@gmail.com', and not 'anonymous'

@9
Scenario: Verify the placeholder text implemented on Comment & CAPTCHA result textboxes so that the user can get a short hint about the expected input format
    When anonymous user visits '/contact' page on 'desktop' device in 'english'
    Then the user should see the following placeholder text implemented on Comment & CAPTCHA textboxes
    * Comment textarea with placeholder text as 'What did you like or dislike?', which has the maximum length of '160' characters
    * CAPTCHA textbox with placeholder text as 'Please enter the result of the CAPTCHA.'

@10
Scenario: Verify that the customer can add or erase feedback in the form of comments
    Given anonymous user visits '/contact' page on 'desktop' device in 'english'
    When anonymous user enters the comment as 'This is a test comment to verify alphanumeric characters -> ABCDabcd12345~!@#$% שלום עולם!' 
    Then anonymous user should see the feedback comment to be populated appropriately in the comment textarea
    When anonymous user deletes the same comment
    Then anonymous user should no longer see the comment feedback in the comment textarea
    But anonymous user should see the error message as 'Please provide a comment.'
  
@11
Scenario: Verify the comment's character count indicator shows the appropriate value based on the characters entered by the customer.
    Given anonymous user visits '/contact' page on 'desktop' device in 'english'
    And anonymous user finds the warning label 'Max. 160 characters' present beneath the comment textarea
    When anonymous user enters the comment as 'abc'
    Then anonymous user should see the character count indicator to show '3/160'
    When anonymous user deletes the same comment
    Then anonymous user should see the character count indicator to show '0/160'
    But if an anonymous user enters a blank space as a comment
    Then anonymous user should see the character count indicator to show '1/160'

@12
Scenario: Verify the customer's ability to write feedback comments using less than 160 characters (<160)
    Given anonymous user visits '/contact' page on 'desktop' device in 'english'
    When anonymous user enters the comment as 'Hello! This is amazing'
    Then anonymous user should see the character count indicator to show '22/160'

@13
Scenario: Verify the customer's ability to write feedback comments using 160 characters (=160)
    Given anonymous user visits '/contact' page on 'desktop' device in 'english'
    When anonymous user enters the comment as 'This is an amazing product. When you click Online Video, you can paste in the embed code for the video you want to add. Word provides header-footer designs too.'
    Then anonymous user should see the character count indicator to show '160/160'

@14
Scenario: Verify that the customer cannot use more than 160 characters to write feedback comments (>160)
    Given anonymous user visits '/contact' page on 'desktop' device in 'english'
    When anonymous user enters the comment such that it exceeds 160 characters limit 'This is an amazing product. When you click Online Video, you can paste in the embed code for the video you want to add. Word provide header-footer designs too. Themes and styles also help to keep your document coordinated.'
    Then anonymous user should still see the character count indicator to show '160/160', and the excess characters should trim off

@15
Scenario: Verify rating slider to show the correct rating value based on the slider movement
    When anonymous user visits '/contact' page on 'desktop' device in 'english'
    And anonymous user performs the following operations to understand what appropriate feedback rating to be selected
    * on moving the rating slider to 1, the feedback value appears as '1★'
    * on moving the rating slider to 2, the feedback value appears as '2★'
    * on moving the rating slider to 3, the feedback value appears as '3★'
    * on moving the rating slider to 4, the feedback value appears as '4★'
    * on moving the rating slider to 5, the feedback value appears as '5★'
    Then anonymous user realizes '5' to be the maximum rating value to be selected

@16
Scenario: Verify captcha to be implemented on feedback form to avoid bot traffic
    When anonymous user visits '/contact' page on 'desktop' device in 'english'
    Then anonymous user sees the CAPTCHA question along with an input textbox to enter the captcha value
    And anonymous user finds the CAPTCHA input textbox to accept only integers

@17
Scenario Outline: Verify captcha field to throw an error on entering invalid input response
    Given anonymous user visits '/contact' page on 'desktop' device in 'english'
    When anonymous user enters an invalid random captcha response '<input>'
    Then it should throw an error 'Invalid CAPTCHA code'

Examples:
    | input     |
    | abc       |
    | !@#       |
    | 1.5       |
    | -2.5      |
    | Abc123!@# |

@18
Scenario Outline: Verify captcha accepts valid input response successfully
    Given anonymous user visits '/contact' page on 'desktop' device in 'english'
    When anonymous user enters a valid random captcha response '<input>'
    Then it should not throw an error 'Invalid CAPTCHA code'

Examples:
    | input |
    | 2     |
    | -2    |

@19
Scenario: Verify new captcha question to appear after every page refresh
    Given anonymous user visits '/contact' page on 'desktop' device in 'english'
    And anonymous user sees the captcha question to appear on the customer feedback form
    When anonymous user reloads the contact page
    Then anonymous user should see the different captcha question to appear after the page reload

@20 @StubCaptchaResult
Scenario: Verify that the customer is unable to submit the feedback form without adding comments 
    Given anonymous user visits '/contact' page on 'desktop' device in 'english'
    When anonymous user decides to keep the comment section blank
    And provides the feedback rating as '3' out of 5 scale
    And answers the CAPTCHA with a correct response '2'
    Then anonymous user should see the Submit button to be disabled

@21 @StubCaptchaResult
Scenario: Verify that the customer is unable to submit the feedback form without adding rating 
    Given anonymous user visits '/contact' page on 'desktop' device in 'english'
    When anonymous user decides not to add the feedback rating 
    And provides the comment as 'This is an amazing product!'
    And answers the CAPTCHA with a correct response '2'
    Then anonymous user should see the Submit button to be disabled

@22 @StubCaptchaResult
Scenario: Verify that the customer is unable to submit the feedback form without answering captcha 
    Given anonymous user visits '/contact' page on 'desktop' device in 'english'
    When anonymous user decides not to answer the CAPTCHA
    And provides the comment as 'This is an amazing product!'
    And provides the feedback rating as '3' out of 5 scale
    Then anonymous user should see the Submit button to be disabled

@23 @StubCaptchaResult
Scenario: Verify that the customer is able to submit the feedback form on answering all required questions 
    Given anonymous user visits '/contact' page on 'desktop' device in 'english'
    When anonymous user enters the following required information present on the feedback form:
    * Comment as 'abc'
    * Rating as '1'
    * correct CAPTCHA response as '2'
    Then anonymous user should see the Submit button to be enabled

@24 @StubCaptchaResult
Scenario Outline: Customer submits the feedback form by adding a rating in range of '1 to 4' out of 5 scale
    Given anonymous user visits '/contact' page on 'desktop' device in 'english'
    And anonymous user enters the following required information present on the feedback form:
    * Comment as 'abc'
    * Rating as '<rating>'
    * correct CAPTCHA response as '2'
    When anonymous user clicks on the Submit button
    Then the feedback form should get submitted successfully and it should show the message as 'Thank you for your feedback.'

Examples:
    | rating |
    | 1      |
    | 2      |
    | 3      |
    | 4      |

# Note:- The below scenario is without the stubbed captcha response. It intercepts the actual '/captcha' api to get the answer.

@25
Scenario: Customer submits the feedback form by adding a rating as 5 out of 5 scale
    Given anonymous user visits '/contact' page on 'desktop' device in 'english'
    And anonymous user enters the following required information present on the feedback form:
    * Comment as 'This is amazing!'
    * Rating as '5'
    * correct CAPTCHA response
    When anonymous user submits the feedbaack form
    Then the feedback form should get submitted successfully and it should show the message as 'Thank you so much for your amazing 5-star feedback!'

@26
Scenario: Customer submits the feedback form by answering wrong CAPTCHA
    Given anonymous user visits '/contact' page on 'desktop' device in 'english'
    And anonymous user enters the following required information present on the feedback form:
    * Comment as 'This is nice!'
    * Rating as '4'
    * incorrect CAPTCHA response '0123456789'
    When anonymous user submits the feedbaack form
    Then the feedback form should not get submitted and it should show an error message as 'Wrong answer to CAPTCHA. Please try again.'
    And anonymous user finds the CAPTCHA input field to get reset

@27 @Smoke
Scenario: Customer submits the feedback form successfully
    Given anonymous user visits '/contact' page on 'desktop' device in 'english'
    And anonymous user enters the following required information present on the feedback form:
    * Comment as 'This is nice!'
    * Rating as '4'
    * correct CAPTCHA response
    When anonymous user submits the feedbaack form
    Then anonymous user finds the comment, rating, and CAPTCHA input fields to get reset after form submission
    But anonymous user should still see the author name as 'anonymous' in readonly state

@28
Scenario: Customer submits the feedback form when the service is down
    Given anonymous user visits '/contact' page on 'desktop' device in 'english'
    And anonymous user enters the complete information - comment as 'abc', rating as '2', and CAPTCHA response as 123
    When user submits the feedback form when the feedback service is down
    Then the feedback form should not get submitted and the feedback service should return 503 error