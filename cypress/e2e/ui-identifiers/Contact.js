/* eslint-disable class-methods-use-this */
class Contact {
  getFeedbackFormHeader() {
    return cy.get('h1');
  }

  getFeedbackFormCard() {
    return cy.get('.mat-card');
  }

  getFeedbackFormAuthorTextInput() {
    return cy.get('#mat-input-1');
  }

  getFeedbackFormCommentTextarea() {
    return cy.get('#comment');
  }

  getFeedbackFormCommentTextareaErrorMessage() {
    return cy.get('mat-error');
  }

  getFeedbackFormCommentTextareaMaxCharacterLimitWarningLabel() {
    return cy.get('mat-hint').first();
  }

  getFeedbackFormCommentTextareaCharacterCountIndicator() {
    return cy.get('mat-hint').last();
  }

  getFeedbackFormRatingSlider() {
    return cy.get('#rating');
  }

  getFeedbackFormCaptchaQuestion() {
    return cy.get('#captcha');
  }

  getFeedbackFormCaptchaTextInput() {
    return cy.get('#captchaControl');
  }

  getFeedbackFormCaptchaTextInputErrorMessage() {
    return cy.get('mat-error');
  }

  getFeedbackFormSubmitButton() {
    return cy.get('#submitButton');
  }

  getFeedbackFormSubmitSmackbarMessage() {
    return cy.get('.mat-simple-snack-bar-content');
  }
}

export default Contact;
