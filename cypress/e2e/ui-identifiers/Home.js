/* eslint-disable class-methods-use-this */
class Home {
  getSideNavigationMenu() {
    return cy.get('[aria-label="Open Sidenav"]');
  }

  getSubcategoryCustomerFeedback() {
    return cy.get('[routerlink="/contact"]').find('.menu-text');
  }

  getCategoryContact() {
    return cy.get('.side-subHeader').eq(1);
  }
}

export default Home;
